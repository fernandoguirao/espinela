<?php
/**
 *	Oxygen WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */

?>



		<!--blog01-->
		<div class="blog">

			<?php
			while(have_posts()):

				the_post();

				get_template_part('tpls/blog-post-single');

			endwhile;
			?>

		</div>
