<?php
/**
 *	Oxygen WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */

$contact_page_blocks = get_field('contact_page_blocks');

get_template_part('tpls/contact-map');

?>
<div class="contact-blocks-env">
<?php

switch($contact_page_blocks)
{
	case "address_contact":
		echo '<h1 class="contact-head">'. get_the_title() .'</h1>';
		echo '<div class="row">';
		get_template_part('tpls/contact-address');
		get_template_part('tpls/contact-form');
		echo '</div>';
		break;

	case "address":
		get_template_part('tpls/contact-address');
		break;

	case "contact":
		get_template_part('tpls/contact-form');
		break;

	default:
		echo '<div class="row">';
		get_template_part('tpls/contact-form');
		get_template_part('tpls/contact-address');
		echo '</div>';
}
?>
</div>
