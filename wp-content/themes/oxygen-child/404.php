<?php
/**
 *	Oxygen WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */

// define("NO_HEADER_MENU", true);
// define("NO_FOOTER_MENU", true);

add_filter('body_class', create_function('$classes', '$classes[] = "not-found"; return $classes;'));

get_header();

?>
<div class="wrapper page-404">

	<div class="center">

		<div class="row">
			<?php
			if(ICL_LANGUAGE_CODE=="es") {
			?>
			<h2>
				Has accedido a un enlace incorrecto.
			</h2>
			<p>
				Puedes volver a nuestra tienda o  <a href="<?php echo site_url(); ?>/contact-us">ponerte en contacto</a> con nosotras si tienes alguna duda.
			</p>
			<a class="btn-404" href="<?php echo site_url(); ?>/boutique">
				Vuelve a la tienda
			</a>
			<?php } else { ?>
				<h2>
					The page you are trying to access doesn't exist.
				</h2>
				<p>
					You can go back to our boutique or <a href="<?php echo site_url(); ?>/en/contact-us">contact with us</a> if you have some question.
				</p>
				<a class="btn-404" href="<?php echo site_url(); ?>/en/boutique">
					Go back to the boutique
				</a>
			<?php } ?>
		</div>

	</div>

</div>

<?php

get_footer();
