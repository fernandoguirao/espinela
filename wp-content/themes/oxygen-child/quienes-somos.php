<?php
/*
	Template Name: quienes-espinela
*/

/**
 *	Oxygen WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */

get_header();

?>

<script type="text/javascript">


/*	function prueba () {
		jQuery('.verticalRev').hide();
		jQuery('.homeRev').show();
		revapi7.revpause();
		revapi6.revredraw();
		revapi6.revresume();
		revapi6.revnext();

	}*/
</script>
<div class="quienes-somos">

	<div class="row sliders" id="slide01">
		<div class="left-image col-md-4 col-sm-3" style="background-image:url('http://espinelashop.com/wp-content/uploads/2015/10/bea.png');">
			<!-- <img src="http://espinelashop.com/wp-content/uploads/2015/10/bea.png" alt="" /> -->
		</div>
		<div class="col-md-5 col-md-offset-5 col-sm-6 col-sm-offset-4 right-content">
			<h1>Beatriz Espinosa</h1>
			<p>
				Beatriz Espinosa is the creative soul of the brand. She is a designer with master’s degree in shoes design studied in Florence at Polimoda school, and besides she has a degree in physiotherapy, this fact adds on top to design and quality the knowledge about the feet needed to develop the proper shoe.
			</p>
			<p>
				Beatriz was born in Valencia, where the brand was also born. She is a creative person in all facets of her life which might be a heritage from her grandfather Julian Espinosa, who was a teacher, painter, calligrapher and poet.
			</p>
			<p>
				 Having a very authentic own personality, class and style, she is a very warm person with a high sensibility who takes a lot of care of details. Being a big admirer of the timeless classic beauty she reinterprets and adapts it to a modern and updated style. To both, personal and professional sides she applies the philosophy of time, details and quality value…”It is worthy to endeavor to spend time in the details…they make a difference and give you back special and unique quality moments”. Based on this principle Beatriz creates her collections and offers a personal advisory service to the clients.
			</p>
			<p class="thumbs">
				<a href="#" class="slide02"><img src="http://espinelashop.com/wp-content/uploads/2015/09/bea1.png" alt="" /></a>
				<a href="#" class="slide03"><img src="http://espinelashop.com/wp-content/uploads/2015/09/atelier.jpg" alt="" /></a>
			</p>
		</div>
	</div>
	<div class="row sliders closed" id="slide02">
		<div class="left-image col-md-4 col-sm-3" style="background-image:url('http://espinelashop.com/wp-content/uploads/2015/10/bea.png');">
			<!-- <img src="http://espinelashop.com/wp-content/uploads/2015/10/bea.png" alt="" /> -->
		</div>
		<div class="col-md-5 col-md-offset-5 col-sm-6 col-sm-offset-4 right-content">
			<h1>Beatriz Espinosa</h1>
			<p>
				Beatriz Espinosa is the creative soul of the brand. She is a designer with master’s degree in shoes design studied in Florence at Polimoda school, and besides she has a degree in physiotherapy, this fact adds on top to design and quality the knowledge about the feet needed to develop the proper shoe.
			</p>
			<p>
				Beatriz was born in Valencia, where the brand was also born. She is a creative person in all facets of her life which might be a heritage from her grandfather Julian Espinosa, who was a teacher, painter, calligrapher and poet.
			</p>
			<p>
				 Having a very authentic own personality, class and style, she is a very warm person with a high sensibility who takes a lot of care of details. Being a big admirer of the timeless classic beauty she reinterprets and adapts it to a modern and updated style. To both, personal and professional sides she applies the philosophy of time, details and quality value…”It is worthy to endeavor to spend time in the details…they make a difference and give you back special and unique quality moments”. Based on this principle Beatriz creates her collections and offers a personal advisory service to the clients.
			</p>
			<p class="thumbs">
				<a href="#" class="slide02"><img src="http://espinelashop.com/wp-content/uploads/2015/09/bea1.png" alt="" /></a>
				<a href="#" class="slide03"><img src="http://espinelashop.com/wp-content/uploads/2015/09/atelier.jpg" alt="" /></a>
			</p>
		</div>
	</div>

</div>

<style>
	.footer-env {
		/*display: none;*/
		background: white;
display: block;
position: relative;
padding-bottom: 18px;
padding-top: 17px;
	}
	.wrapper,.wrapper .main {
		background-color:black !important;
	}
	html {
	    margin-top:-12px !important;
			background-color:white;
	}
	.coleccion {
		display: none;
	}

	.oxygen-top-menu .wrapper .main-menu-top .cart-ribbon {
		width: 212px;
		right: 0;
		position: absolute;
		top: 0;
		visibility: hidden;
	}

	.oxygen-top-menu.page-template-slider-home .wrapper .main-menu-top .logo {
		position: fixed;
		left: 42px;
		top:16px;
	}
	@media(max-width:991px){
		.oxygen-top-menu>.wrapper>.main-menu-top.sticky {
			top: 51px!important;
			padding-left:0;
			padding-right:0;
		}
	}
</style>
<script type="text/javascript">
/*
jQuery(document).ready(function() {
	revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
		console.log("video play");
		// data.video.pause();
		//data.video - The Video API to Manage Video functions
		//data.videotype - youtube, vimeo, html5
		//data.settings - Video Settings});
	});

revapi11.bind("revolution.slide.layeraction",function (e,data) {
	//data.eventtype - Layer Action (enterstage, enteredstage, leavestage,leftstage)
	//data.layertype - Layer Type (image,video,html)
	//data.layersettings - Default Settings for Layer
	//data.layer - Layer as jQuery Object

	console.log(data.layer);
	});
});*/



jQuery(document).ready(function() {

	jQuery('.slide02').click(function(e) {
		e.preventDefault();
		jQuery('#slide02').toggleClass('closed');
		jQuery('#slide01').toggleClass('closed');
	})

	if(typeof revapi11 === 'undefined') return;

	var up;
	var down;
	var totalSlides;
	var thumbs;
	var bullets;
	var mainMenu;
	var layer = {};
	var layers = new Array();
	var video;
	var enisa = jQuery('.enisa');
	var cookies = jQuery('.cookies');

	function uiOnScroll(order){
		if (order === 'hide'){
			up.css('visibility', 'hidden');
			down.css('visibility', 'hidden');
			thumbs.css('visibility', 'hidden');
			bullets.css('visibility', 'hidden');
			cookies.css('opacity','.6');
			enisa.css('opacity','0');
			mainMenu.addClass('first-slide');
			mainMenu.addClass('sticky');
			// mainMenu.removeClass('sticky-menu');
			// mainMenu.addClass('sticky');

		}
		if (order === 'show'){
			up.css('visibility', 'visible');
			down.css('visibility', 'visible');
			thumbs.css('visibility', 'visible');
			bullets.css('visibility', 'visible');
			mainMenu.removeClass('first-slide');
			enisa.css('opacity','.6');
			cookies.css('opacity','0');
			// mainMenu.addClass('sticky-menu');
			// mainMenu.removeClass('sticky');
		}
	}


/*
	function controlVid(order){
		if (order === 'play') {

			revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
				console.log(video);
				video = data.video;
				video.play();
			});
		} else {
			revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
				video = data.video;
				video.pause();
			});

		}*/
/*			if (order === 'play') {
				console.log('playing');
				video.play();
			} else {
				console.log('pausing');
				video.pause();
			}*/
			// data.video.pause();
			//data.video - The Video API to Manage Video functions
			//data.videotype - youtube, vimeo, html5
			//data.settings - Video Settings});


function controlVid(timing){

		revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
			console.log('bind');
			video = data.video;
			console.log('videopaused')
			video.pause();
			setTimeout(resume, timing);
			function resume() {
				console.log('resume');
				video.play();
				enisa.css('opacity','.6');
				cookies.css('opacity','0');
			}

			revapi11.unbind("revolution.slide.onvideoplay");

		});
}




	function showMenu(){
		revapi11.bind("revolution.slide.layeraction",function (e,data) {
			layer = data.layer;

			console.log(data.eventtype);
			if (layer.context.id === 'slide-28-layer-5' && data.eventtype === 'enterstage') {
					mainMenu.removeClass('first-slide');
					revapi11.unbind("revolution.slide.layeraction");
			}

			//data.eventtype - Layer Action (enterstage, enteredstage, leavestage,leftstage)
			//data.layertype - Layer Type (image,video,html)
			//data.layersettings - Default Settings for Layer
			//data.layer - Layer as jQuery Object
		});
	}


	revapi11.bind('revolution.slide.onloaded', function() {

		// revapi12.revpause();

		totalSlides = revapi11.revmaxslide();
		up = jQuery('.tp-leftarrow');
		down = jQuery('.tp-rightarrow');
		thumbs = jQuery('.tp-thumbs');
		bullets = jQuery('.tp-bullets');
		mainMenu = jQuery('.main-menu-top');
		mainMenu.addClass('loaded');

		uiOnScroll('hide');

	});



	// function that will fire every time a slide loads
	revapi11.bind('revolution.slide.onchange', function(event, data) {

		// the index of the current slide
		var currentSlide = data.slideIndex;
		console.log(currentSlide);

		if(currentSlide === 1) {

			uiOnScroll('hide');

			controlVid(3000);

			showMenu();

		}

		else if(currentSlide === totalSlides) {

			uiOnScroll('show');

		}

		else if (currentSlide < 2||currentSlide > 13) {
			uiOnScroll('show');
			jQuery('.tparrows').hide();
		} else {
			jQuery('.tparrows').show();
			uiOnScroll('show');
			if (jQuery(window).width() < 960) {
				cookies.css('opacity','0');
			}
		}

	});

	// MENÚ LATERAL

	var slideID;
	jQuery('.a-slide').click(function(){
		// backHome();
		slideID = jQuery(this).data('slide');
		// jQuery('.active').removeClass('active');
		// jQuery(this).parent().addClass('active');
		revapi11.revshowslide(slideID);

	})

	var activo;
	revapi11.bind("revolution.slide.onchange",function (e,data) {
		jQuery('.active').removeClass('active');
		activo = jQuery(".a-slide[data-slide='" + data.slideIndex + "']");
		activo.parent().addClass('active');
		//data.currentslide - Current  Slide as jQuery Object
		//data.prevslide - Previous Slide as jQuery Object
		/*if (data.slideIndex === 2 ){
			coleccionStart();
		}*/
	});

	// MOSTRAR COLECCIÓN
	/*var home = jQuery('.homeMain');
	var coleccion = jQuery('.coleccion');

	function coleccionStart () {
		home.hide();
		coleccion.show();
		revapi11.revpause();
		revapi12.revredraw();
		revapi12.revresume();
	}
	function backHome () {
		if(coleccion.is(":visible")){
			home.show();
			revapi12.revpause();
			revapi11.revredraw();
			revapi11.revresume();
			coleccion.hide();
		}

	}*/

/*	revapi12.bind('revolution.slide.onchange', function(event, data) {

		var currentSlide = data.slideIndex;
		console.log(currentSlide);
		var arrows = jQuery('.tparrows');
		if(currentSlide === 1) {
			arrows.hide();
		} else {
			arrows.show();
		}

	});*/

});



</script>
<?php
get_footer();
