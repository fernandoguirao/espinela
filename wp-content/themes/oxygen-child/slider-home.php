<?php
/*
	Template Name: home-espinela
*/

/**
 *	Oxygen WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */

get_header();

?>
<?php
if(ICL_LANGUAGE_CODE=="es") {
?>
<div class="latMenu">
	<ul>
		<li class="active">
			<a href="#" data-slide="1" class="a-slide">
				<p>
					Espinela
				</p>
			</a>
		</li>
		<li>
			<a href="#" data-slide="2" class="a-slide">
				<p>
					Campaña
				</p>
			</a>
		</li>
		<li>
			<a href="#" data-slide="14" class="a-slide">
				<p>
					Backstage
				</p>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url(); ?>/boutique/">
				<p>
					Boutique
				</p>
			</a>
		</li>
		<li>
			<li>
				<a href="<?php echo site_url(); ?>/customizer/">
					<p>
						Personalización
					</p>
				</a>
			</li>
			<li>
			<a href="<?php echo site_url(); ?>/espinela-world/">
				<p>
					Espinela World
				</p>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url(); ?>/contact-us/">
				<p>
					Contacto
				</p>
			</a>
		</li>
	</ul>
</div>
<?php
} else {
?>
<div class="latMenu">
	<ul>
		<li class="active">
			<a href="#" data-slide="1" class="a-slide">
				<p>
					Espinela
				</p>
			</a>
		</li>
		<li>
			<a href="#" data-slide="2" class="a-slide">
				<p>
					Campaign
				</p>
			</a>
		</li>
		<li>
			<a href="#" data-slide="14" class="a-slide">
				<p>
					Backstage
				</p>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url(); ?>/en/boutique/">
				<p>
					Boutique
				</p>
			</a>
		</li>
		<li>
			<li>
				<a href="<?php echo site_url(); ?>/en/customizer/">
					<p>
						Customize
					</p>
				</a>
			</li>
			<li>
			<a href="<?php echo site_url(); ?>/en/espinela-world/">
				<p>
					Espinela World
				</p>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url(); ?>/en/contact-us/">
				<p>
					Contact
				</p>
			</a>
		</li>
	</ul>
</div>
<?php
}
?>
<!--
<div class="enisa">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_enisa.png" alt="">
</div>
-->
<?php
if(ICL_LANGUAGE_CODE=="es") {
?>
<div class="cookies">
	<p>
		Para que esta página funcione necesitamos cookies. Si sigues navegando aceptas que almacenemos datos en ellos.
	</p>
</div>
<?php
} else {
?>
<div class="cookies">
	<p>
		For this site to work need cookies. If you continue browsing agree that we store data on them.
	</p>
</div>
<?php
}
?>

<?php
if(ICL_LANGUAGE_CODE=="es") {
?>
<div class="language">
	<a href="#" class="volume volume-on">
		<i class="fa fa-volume-up"></i>
		<i class="fa fa-volume-off"></i>
	</a>
	<a href="http://espinelashop.com/en" class="inactive">
		EN
	</a>
	<a href="http://espinelashop.com/" class="active">
		ES
	</a>
</div>
<?php
} else {
?>
<div class="language">
	<a href="#" class="volume volume-on">
		<i class="fa fa-volume-up"></i>
		<i class="fa fa-volume-off"></i>
	</a>
	<a href="http://espinelashop.com/en" class="active">
		EN
	</a>
	<a href="http://espinelashop.com/" class="inactive">
		ES
	</a>
</div>
<?php
}
?>


<div class="toCampaign toSlide">

		<?php
		if(ICL_LANGUAGE_CODE=="es") {
		?>
		<a href="http://espinelashop.com/boutique">
		Ir a la boutique
		<i class="pe-7s-angle-down"></i>
		</a>
		<?php
		} else {
		?>
		<a href="http://espinelashop.com/en/boutique">
		Boutique
		<i class="pe-7s-angle-down"></i>
		</a>
		<?php
		}
		?>

</div>
<?php
/*<div class="toQuienes toSlide">
	<a href="#" data-slide="14" class="a-slide">
		<?php
		if(ICL_LANGUAGE_CODE=="es") {
		?>
		Quiénes somos
		<?php
		} else {
		?>
		Who we are
		<?php
		}
		?>
		<i class="pe-7s-angle-down"></i>
	</a>
</div> */
?>
<div class="toBea toSlide">
	<a href="#" data-slide="15" class="a-slide">
		Bea Espinosa
		<i class="pe-7s-angle-down"></i>
	</a>
</div>

<div class="toAtelier toSlide">
	<a href="#" data-slide="16" class="a-slide">
		Atelier Espinela
		<i class="pe-7s-angle-down"></i>
	</a>
</div>
<div class="toBoutique toSlide">
	<?php
	if(ICL_LANGUAGE_CODE=="es") {
	?>
	<a href="http://espinelashop.com/boutique">
	Ir a la boutique
	<i class="pe-7s-angle-down"></i>
	</a>
	<?php
	} else {
	?>
	<a href="http://espinelashop.com/en/boutique">
	Boutique
	<i class="pe-7s-angle-down"></i>
	</a>
	<?php
	}
	?>
</div>


<script type="text/javascript">


/*	function prueba () {
		jQuery('.verticalRev').hide();
		jQuery('.homeRev').show();
		revapi7.revpause();
		revapi6.revredraw();
		revapi6.revresume();
		revapi6.revnext();

	}*/
</script>
<?php

echo '<div class="homeMain">'.do_shortcode('[rev_slider alias="home-main"]').'</div>';

// echo '<div class="coleccion">'.do_shortcode('[rev_slider alias="coleccion"]').'</div>';
?>

<style>
	.footer-env {
		display: none;
	}
	.wrapper,.wrapper .main {
		background-color:black !important;
	}
	html {
	    margin-top:-12px !important;
	}
	.coleccion {
		display: none;
	}

	.oxygen-top-menu .wrapper .main-menu-top .cart-ribbon {
		width: 212px;
		right: 0;
		position: absolute;
		top: 0;
		visibility: hidden;
	}

	.oxygen-top-menu.page-template-slider-home .wrapper .main-menu-top .logo {
		position: fixed;
		left: 42px;
		top:16px;
	}
	@media(max-width:991px){
		.oxygen-top-menu>.wrapper>.main-menu-top.sticky {
			padding-left:0;
			padding-right:0;
		}
	}
</style>
<script type="text/javascript">
/*
jQuery(document).ready(function() {
	revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
		console.log("video play");
		// data.video.pause();
		//data.video - The Video API to Manage Video functions
		//data.videotype - youtube, vimeo, html5
		//data.settings - Video Settings});
	});

revapi11.bind("revolution.slide.layeraction",function (e,data) {
	//data.eventtype - Layer Action (enterstage, enteredstage, leavestage,leftstage)
	//data.layertype - Layer Type (image,video,html)
	//data.layersettings - Default Settings for Layer
	//data.layer - Layer as jQuery Object

	console.log(data.layer);
	});
});*/



jQuery(document).ready(function() {



	if(typeof revapi11 === 'undefined') return;

	var up;
	var down;
	var totalSlides;
	var thumbs;
	var bullets;
	var mainMenu;
	var layer = {};
	var layers = new Array();
	var video;
	// var enisa = jQuery('.enisa');
	var toCampaign = jQuery('.toCampaign');
	// var toQuienes = jQuery('.toQuienes');
	var toBoutique = jQuery('.toBoutique');
	var toBea = jQuery('.toBea');
	var toAtelier = jQuery('.toAtelier');
	var cookies = jQuery('.cookies');
	var language = jQuery('.language');
	var mainParagraph = jQuery('.main-paragraph');
	var volume = jQuery('.volume');

	function uiOnScroll(order){
		if (order === 'hide'){
			up.css('visibility', 'hidden');
			down.css('visibility', 'hidden');
			thumbs.css('visibility', 'hidden');
			bullets.css('visibility', 'hidden');
			cookies.css('opacity','.6');
			// enisa.css('opacity','0');
			toCampaign.css('opacity','0');
			mainMenu.addClass('first-slide');
			mainMenu.addClass('sticky');

			// mainMenu.removeClass('sticky-menu');
			// mainMenu.addClass('sticky');

		}
		if (order === 'show'){
			up.css('visibility', 'visible');
			down.css('visibility', 'visible');
			thumbs.css('visibility', 'visible');
			bullets.css('visibility', 'visible');
			mainMenu.removeClass('first-slide');
			toCampaign.css('opacity','.8');
			cookies.css('opacity','0');
			// mainMenu.addClass('sticky-menu');
			// mainMenu.removeClass('sticky');
		}
	}


/*
	function controlVid(order){
		if (order === 'play') {

			revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
				console.log(video);
				video = data.video;
				video.play();
			});
		} else {
			revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
				video = data.video;
				video.pause();
			});

		}*/
/*			if (order === 'play') {
				console.log('playing');
				video.play();
			} else {
				console.log('pausing');
				video.pause();
			}*/
			// data.video.pause();
			//data.video - The Video API to Manage Video functions
			//data.videotype - youtube, vimeo, html5
			//data.settings - Video Settings});


function controlVid(timing){

		revapi11.bind("revolution.slide.onvideoplay",function (e,data) {
			console.log('bind');
			video = data.video;
			console.log('videopaused')
			video.pause();
			setTimeout(resume, timing);
			function resume() {
				console.log('resume');
				video.play();
				toCampaign.css('opacity','.8');
				cookies.css('opacity','0');
				mainParagraph.fadeIn();
				volume.fadeIn("slow");
			}

			volume.click(function(e){
				e.preventDefault();
				volume.toggleClass('volume-on');
				if (jQuery(this).hasClass('volume-on')){
					video.muted = false;
				} else {
					video.muted = true;
				}

			})

			revapi11.unbind("revolution.slide.onvideoplay");

			setTimeout(function () {

				mainParagraph.fadeOut(1000);
				volume.fadeOut(200);
			}, 56000);

		});
}




	function showMenu(){
		revapi11.bind("revolution.slide.layeraction",function (e,data) {
			layer = data.layer;


			console.log(data.eventtype);
			if (layer.context.id === 'slide-28-layer-5' && data.eventtype === 'enterstage' || layer.context.id === 'slide-88-layer-5' && data.eventtype === 'enterstage') {
					mainMenu.removeClass('first-slide');
					revapi11.unbind("revolution.slide.layeraction");
					cookies.css('opacity','0');
					language.css('opacity','1');
			}

			//data.eventtype - Layer Action (enterstage, enteredstage, leavestage,leftstage)
			//data.layertype - Layer Type (image,video,html)
			//data.layersettings - Default Settings for Layer
			//data.layer - Layer as jQuery Object
		});
	}


	revapi11.bind('revolution.slide.onloaded', function() {

		// revapi12.revpause();

		totalSlides = revapi11.revmaxslide();
		up = jQuery('.tp-leftarrow');
		down = jQuery('.tp-rightarrow');
		thumbs = jQuery('.tp-thumbs');
		bullets = jQuery('.tp-bullets');
		mainMenu = jQuery('.main-menu-top');
		mainMenu.addClass('loaded');

		uiOnScroll('hide');

	});



	// function that will fire every time a slide loads
	revapi11.bind('revolution.slide.onchange', function(event, data) {

		// the index of the current slide
		var currentSlide = data.slideIndex;
		console.log(currentSlide);

		if(currentSlide === 1) {

			uiOnScroll('hide');

			controlVid(3000);

			showMenu();

		}

		else if(currentSlide === totalSlides) {

			uiOnScroll('show');

		}

		else if (currentSlide < 2||currentSlide > 13) {
			uiOnScroll('show');
			jQuery('.tparrows').hide();
		} else {
			jQuery('.tparrows').show();
			uiOnScroll('show');
			if (jQuery(window).width() < 960) {
				cookies.css('opacity','0');
			}
		}

		if (currentSlide === 1) {
			toCampaign.fadeIn();
			// toQuienes.fadeOut();
			toAtelier.fadeOut('0');
			toBea.fadeOut('0');
			toBoutique.fadeOut('0');
		} else {
			// toQuienes.css('opacity','.7');
			toAtelier.css('opacity','.7');
			toBea.css('opacity','.7');
			toBoutique.css('opacity','.7');
		}
		if (currentSlide > 1 && currentSlide < 14) {
			toCampaign.fadeOut();
			// toQuienes.fadeIn();
			toAtelier.fadeOut();
			toBea.fadeOut();
			toBoutique.fadeOut();
			// enisa.css('opacity','.6');
		}
		if (currentSlide === 14) {
			toCampaign.fadeOut();
			// toQuienes.fadeOut();
			toAtelier.fadeOut();
			toBea.delay(500).fadeIn();
			toBoutique.fadeOut();

		}
		if (currentSlide === 15) {
			toCampaign.fadeOut();
			// toQuienes.fadeOut();
			toAtelier.delay(500).fadeIn();
			toBea.fadeOut();
			toBoutique.fadeOut();
		}
		if (currentSlide === 16) {
			toCampaign.fadeOut();
			// toQuienes.fadeOut();
			toAtelier.fadeOut();
			toBea.fadeOut();
			toBoutique.delay(500).fadeIn();
		}



	});

	// MENÚ LATERAL

	var slideID;
	jQuery('.a-slide').click(function(){
		// backHome();
		slideID = jQuery(this).data('slide');
		// jQuery('.active').removeClass('active');
		// jQuery(this).parent().addClass('active');
		revapi11.revshowslide(slideID);

	})

	var activo;
	revapi11.bind("revolution.slide.onchange",function (e,data) {
		jQuery('.active').removeClass('active');
		activo = jQuery(".a-slide[data-slide='" + data.slideIndex + "']");
		activo.parent().addClass('active');
		//data.currentslide - Current  Slide as jQuery Object
		//data.prevslide - Previous Slide as jQuery Object
		/*if (data.slideIndex === 2 ){
			coleccionStart();
		}*/
	});

	// MOSTRAR COLECCIÓN
	/*var home = jQuery('.homeMain');
	var coleccion = jQuery('.coleccion');

	function coleccionStart () {
		home.hide();
		coleccion.show();
		revapi11.revpause();
		revapi12.revredraw();
		revapi12.revresume();
	}
	function backHome () {
		if(coleccion.is(":visible")){
			home.show();
			revapi12.revpause();
			revapi11.revredraw();
			revapi11.revresume();
			coleccion.hide();
		}

	}*/

/*	revapi12.bind('revolution.slide.onchange', function(event, data) {

		var currentSlide = data.slideIndex;
		console.log(currentSlide);
		var arrows = jQuery('.tparrows');
		if(currentSlide === 1) {
			arrows.hide();
		} else {
			arrows.show();
		}

	});*/


});




</script>
<?php
get_footer();
