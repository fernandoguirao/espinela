<?php
/**
 *	Oxygen WordPress Theme
 *
 *	Laborator.co
 *	www.laborator.co
 */


add_action('wp_enqueue_scripts', 'enqueue_childtheme_scripts', 1000);

function enqueue_childtheme_scripts()
{
	wp_enqueue_style('oxygen-child', get_stylesheet_directory_uri() . '/style.css');
}
/*
* wc_remove_related_products
*
* Clear the query arguments for related products so none show.
* Add this code to your theme functions.php file.
*/
function wc_remove_related_products( $args ) {
  return array();
}

add_filter('woocommerce_related_products_args','wc_remove_related_products', 10);

require get_stylesheet_directory() . '/inc/enqueue-assets.php';

//[foobar]
function novias_func(){
	return "foo and bar";
}
add_shortcode( 'novias', 'novias_func' );
/*
function price_array($price){
	$del = array('<span class="amount">', '</span>','<del>','<ins>');
	$price = str_replace($del, '', $price);
	$price = str_replace('</del>', '|', $price);
	$price = str_replace('</ins>', '|', $price);
	$price_arr = explode('|', $price);
	$price_arr = array_filter($price_arr);
	return $price_arr;
}

add_filter( 'woocommerce_get_price_html', 'price_array', 100, 2 );
*/

add_filter('body_class','adminClass');

function adminClass($classes = '') {
	$classes[] = 'logged-in admin-bar';
	if (!is_user_logged_in()){
		$classes[] = 'not-logged';
	}
	if(ICL_LANGUAGE_CODE=="es") {
		$classes[] = 'es-es';
	} else {
		$classes[] = 'en-en';
	}
	return $classes;
}