</div>
<?php
$img_name = 'http://espinelashop.com/wp-content/images_campaign/' . strtolower( get_the_title() ) . '.jpg';
$img_name = str_replace(' ', '-', $img_name);
$img_name = str_replace('é', 'e', $img_name);
$img_name = str_replace('É', 'e', $img_name);
/*
echo '<br><br><br>';
if ( file_exists( $img_name ) ) {
    echo "El fichero $img_name existe";
} else {
    echo "El fichero $img_name no existe";
}
*/
?>
<style>
.texto_header {
	color: rgb(255, 255, 255); 
	font-family: Bodoniz, serif;
	letter-spacing: 2px; 
	font-weight: 600;
	height:370px !important;
	vertical-align:middle;
}
<?php
if($_SERVER['REQUEST_URI']=="/boutique/" || $_SERVER['REQUEST_URI']=="/en/boutique/") {
?>
.widget_text .textwidget .giftcard {
	display:block;
}
<?php
} else {
?>
.widget_text .textwidget .giftcard {
	display:none;
}
<?php
}
?>
</style>
<?php
if($_SERVER['REQUEST_URI']!="/boutique/" && $_SERVER['REQUEST_URI']!="/en/boutique/") {
	global $wp_query;
	// get the query object
	$cat_obj = $wp_query->get_queried_object();
	  
	//print_r($cat_obj->post_name);
	  
	if($cat_obj)    {
		$category_name = $cat_obj->name;
		$category_desc = $cat_obj->description;
		$category_ID  = $cat_obj->term_id;
	}
?>
    <div class="wpb_wrapper">
        <div class="forcefullwidth_wrapper_tp_banner" style="position:relative;width:100%;height:370px !important;margin-top:0px;margin-bottom:25px;">
<?php
	if ( $cat_obj->post_name == 'gift' ) {
		//echo "AAA";
?>
        	<div style="width: 100%; height: 100%; visibility: hidden !important; z-index: 0; background-color: #FFFFFF !important;">	
<?php
	} else {
		//echo "BBB";
?>
        	<div style="width: 100%; height: 100%; visibility: inherit; z-index: 999; background-image: url(<?php echo $img_name ?>); background-color: #FFFFFF; background-size: cover; background-position: 50% 100%; background-repeat: no-repeat;" src="<?php echo $img_name ?>">
<?php
	}
?>
                <div align="center" id="texto_dinamico" class="texto_header">
                    <?php 
                    global $vengodecategoria;
                    if($_SERVER['REQUEST_URI']=="/boutique/" || $_SERVER['REQUEST_URI']=="/en/boutique/") {
                        echo "Boutique";
                    } else {
                        if($vengodecategoria=="1") {
                            //woocommerce_page_title();
                        } else {
                            //echo the_title();
                            echo '<div class="texto_header_description">';
                            the_content();
                            echo '</div>';
                        }
                    }
                    $vengodecategoria="";
                    ?>
                </div>
            
            </div>
        </div>
    </div>
<?php
}
?>
<div class="main">
<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo '<div class="laborator-woocommerce shop">';

/*
$template = get_option( 'template' );

switch( $template ) 
{
	default :
		echo '<div class="laborator-woocommerce shop">';
		break;
}
*/
      