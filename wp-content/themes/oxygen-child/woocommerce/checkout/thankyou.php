<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(ICL_LANGUAGE_CODE=="es") {
	$Thank = '¡Gracias por comprar en Espinela! Estamos preparando tu pedido. Dentro de muy poco unos zapatos muy especiales llamarán a tu puerta.';
	$OrderTit = 'Pedido:';
} else {
	$Thank = 'Thank you for shopping in Espinela! We are preparing your order. Very soon, a very special pair of shoes will be arriving at your door.';
	$OrderTit = 'Order:';
}

# start: modified by Arlind Nushi
do_action('laborator_woocommerce_before_wrapper');

?>

<div class="row <?php if ( $order->payment_method_title === 'Transferencia bancaria' || $order->payment_method_title === 'BACS' ) { ?>transferencia<?php } ?>" style="margin-top:40px !important">
	<div class="col-md-12 checkout-finish">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/thanks.jpg" alt="" />
<?php
# end: modified by Arlind Nushi

if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></p>

		<p><?php
			if ( is_user_logged_in() )
				_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
			else
				_e( 'Please attempt your purchase again.', 'woocommerce' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>
    	<?php if ( $order->payment_method_title != 'Transferencia bancaria' && $order->payment_method_title != 'BACS' ) { ?>
		<div class="woocommerce-success" style="text-transform:inherit !important">
			<?php _e( $Thank, 'woocommerce' ); ?>
		</div>
		<?php } ?>
		<ul class="order_details header">
			<li class="order">
				<?php _e( $OrderTit, 'oxygen' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="date">
				<?php _e( 'Date:', 'woocommerce' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php _e( 'Total:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php _e( 'Payment Method:', 'woocommerce' ); ?>
				<strong><?php echo $order->payment_method_title; ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<div class="white-block block-padd first-row" style="text-transform:inherit !important">
		<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	</div>
	
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

<?php else : ?>

	<div class="woocommerce-success">
		<?php _e( 'Thank you. Your order has been received.', 'woocommerce' ); ?>
	</div>
<?php endif; ?>

<?php
# start: modified by Arlind Nushi
?>
	</div>
</div>
<?php
do_action('laborator_woocommerce_after_wrapper');
# end: modified by Arlind Nushi
?>