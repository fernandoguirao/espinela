<?php
/**
 * Loop Price
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


if(ICL_LANGUAGE_CODE=="es") {
	$PVariable = 'Opciones';
	$PGroup = 'Seleccionar';
	$PDefault = 'Añadir al Carrito';
} else {
	$PVariable = 'Options';
	$PGroup = 'Selecct';
	$PDefault = 'Add to Cart';
}

global $product;

//$price_html = $product->get_price_html();

//print_r( $price_html );

//$price_html_array = price_array($price_html);

//echo ' - YYY - ' . $price_html;
/*
function price_array_A($price){
	$del = array('<span class="amount">', '</span>','<del>','<ins>');
	$price = str_replace($del, '', $price);
	$price = str_replace('</del>', '|', $price);
	$price = str_replace('</ins>', '|', $price);
	$price_arr = explode('|', $price);
	$price_arr = array_filter($price_arr);
	return $price_arr;
}
*/
//echo ' -  - ';
//print_r( $price_html_array );

?>
<?php /*?>
<?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="price"><?php echo $price_html; ?></span>
<?php endif; ?>
<?php */?>
<?php if ( $price_html = $product->min_variation_price ) : ?>
    <span class="price" style="color: #C7C7C7 !important;font-size: 12px;font-weight: 300;"><?php echo $price_html . ',00€'; ?></span>
<?php endif; ?>

<?php
# start: modified by Arlind Nushi
if( ! get_data('shop_add_to_cart_listing'))
	return;
# end: modified by Arlind Nushi
?>

<?php if(is_catalog_mode()) return; ?>


<?php if($product->is_type('variable')): ?>
<a class="add-to-cart-btn entypo-list-add" data-toggle="tooltip" data-placement="left" title="<?php _e($PVariable, 'oxygen'); ?>" href="<?php echo $product->get_permalink(); ?>"></a>

<?php elseif($product->is_type('grouped')): ?>
<a class="add-to-cart-btn entypo-list-add" data-toggle="tooltip" data-placement="bottom" title="<?php _e($PGroup, 'oxygen'); ?>" href="<?php echo $product->get_permalink(); ?>"></a>

<?php elseif($product->is_type('external')): ?>
<a class="add-to-cart-btn entypo-export" data-toggle="tooltip" data-placement="bottom" title="<?php echo $product->single_add_to_cart_text(); ?>" href="<?php echo $product->get_product_url(); ?>" target="_blank"></a>

<?php else: ?>

<a class="add-to-cart-btn add-to-cart glyphicon glyphicon-plus-sign" data-id="<?php echo $product->post->ID; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php _e($PDefault, 'oxygen'); ?>" href="#">
	<span class="glyphicon glyphicon-ok-sign"></span>
</a>
<?php endif; ?>