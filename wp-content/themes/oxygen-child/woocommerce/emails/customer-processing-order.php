<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action('woocommerce_email_header', $email_heading); ?>

<?php /*?><p><?php _e( "Your order has been received and is now being processed. Your order details are shown below for your reference:", 'woocommerce' ); ?></p><?php */?>

<?php
if(ICL_LANGUAGE_CODE=="es") {
?>
<p>Gracias por comprar en espinela.com, nos encanta que confíes en nosotras. Con éste email te confirmamos que hemos recibido tu pedido:</p>
<?php
} else {
?>
<p>Thanks for shopping at espinela.com, we love you to trust us. With this email we will confirm that we have received your order:</p>
<?php
}
?>
<br>

<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'processing' ) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>
<br>
<?php
if(ICL_LANGUAGE_CODE=="es") {
?>
<p>¡Esperamos que disfrutes mucho de tus Espinela! En muy pocos días te llegarán a casa. Para lo que necesites no dudes en contactarnos, estaremos encantadas de ayudarte.</p>
<br><br>
<p>Un saludo,<br>
Equipo Espinela.</p>
<?php
} else {
?>
<p>We hope you enjoy your Espinela much! In a few days they will reach home. For what you need please contact us, we will be happy to help you.</p>
<br><br>
<p>Regards,<br>
Espinela Team.</p>
<?php
}
?>

<?php do_action( 'woocommerce_email_footer' ); ?>
