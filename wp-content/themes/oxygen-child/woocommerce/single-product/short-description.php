<?php
/**
 * Single product short description
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;

if ( ! $post->post_excerpt ) return;
?>
<div itemprop="description" style="font-family:'Bodoniz',serif !important;">
	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
</div>
<hr style="color:#c0c0c0; margin-top:16px; margin-bottom:2px;">
<br>
<div class="tablaproduct123">     
    <table class="shop_attributes" width="100%">
    
    <?php 
        $attributes = $product->get_attributes();
    
        foreach ( $attributes as $attribute ) :
        if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) {
            continue;
        } else {
            $has_row = true;
        }
        ?>
        <tr>
            <td align="left" width="112"><p style="font-family:'Bodoniz',serif !important; font-size:12px !important; text-transform:uppercase !important"><?php echo wc_attribute_label( $attribute['name'] ); ?></p></td>
            <td align="right" style="padding-left:8px; padding-right:6px;"><?php
                if ( $attribute['is_taxonomy'] ) {
    
                    $values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
                    echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );
    
                } else {
    
                    // Convert pipes to commas and display values
                    $values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
                    echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );
    
                }
            ?></td>
        </tr>
    <?php endforeach; ?>
    
    </table>  
</div>
<!--<hr style="color:#c0c0c0; margin-top:10px; margin-bottom:0px;">-->
<br>
