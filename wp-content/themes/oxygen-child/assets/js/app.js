jQuery(document).ready(function(){

    if(jQuery('.popovers').length > 0) {
        jQuery('[data-toggle="popover"]').popover();
    }
    // IF LENGTH ESPINELA WORLD
    if (jQuery('.page-id-1794').length > 0 || jQuery('.page-id-2508').length > 0){
        refreshId = setInterval(auto_refresh, 1000);
    }
    // If página de celebrities o imaginario
    if (jQuery('.postid-2220,.postid-2216,.postid-2855,.postid-2853').length > 0){
        refreshId = setInterval(auto_click, 1000);
    }
    // IF LENGTH BLOG POST
    if (jQuery('.single-post').length > 0){
        changeImagePost();
    }
    // IF LENGTH PRODUCTO
    if (jQuery('.product-single').length > 0){
        jQuery('.person1').insertBefore('#main-image-slider');
        var plantilla = jQuery('.plantza');
		//plantilla.text('Descarga nuestra plantilla para averiguar tu talla de Espinela');
		if (window.location.href.indexOf("/en/") > -1) {
			plantilla.text('Download our template to determine your Espinela size');
		} else {
			plantilla.text('Descarga nuestra plantilla para averiguar tu talla de Espinela');
		}
        plantilla.insertAfter('.thumbnails');
    }
    if(jQuery('.woocommerce-page').length > 0){
        jQuery('.product-categories .cat-item-110 .children').prev().attr('href','http://espinela.com/boutique/');
        jQuery('.cat-item-95, .cat-item-159').before('<li class="cat-item current-cat"><a href="http://espinela.com/boutique/">Todos</a></li>');
    }
});

function auto_refresh(){
    if (jQuery('a:contains("Imaginario")')){
        // clearInterval(refreshId);
        // console.log('encontrado');
        var celebrities = jQuery('a:contains("Celebrities")');
        var imaginario = jQuery('a:contains("Imaginario")');

        imaginario.parent().parent().parent().parent().addClass('imaginario');
        imaginario.css('display','none');
        celebrities.parent().parent().parent().parent().addClass('celebrities');
        celebrities.css('display','none');

        jQuery('.ff-posts').addClass('blog-post');
        jQuery('.ff-posts.imaginario,.ff-posts.celebrities').removeClass('blog-post');
    } else {
        console.log('buscando');
    }
    if (jQuery('.ff-item').hasClass('imaginario')){
        console.log('hallado');
        clearInterval(refreshId);
    }
}

function auto_click(){
    if (jQuery('.ff-item-cont img')){
        // clearInterval(refreshId);
        console.log('encontrado');
        jQuery('.ff-item-cont img').parent().parent().parent().parent().first().addClass('primero');
    } else {
        console.log('buscando');
    }
    if (jQuery('.primero').length > 0){
        console.log('hallado');
        clearInterval(refreshId);
        jQuery('.primero div').trigger('click');
    }
}

function changeImagePost() {
    var imagen = jQuery('.post_img.nivo a img');
    var contenedor = jQuery('.post_img.nivo');
    var src = imagen.attr('src');
    imagen.hide();
    console.log(src);
    contenedor.css('background-image','url('+src+')');
    contenedor.insertAfter('.main-menu-top');
}
