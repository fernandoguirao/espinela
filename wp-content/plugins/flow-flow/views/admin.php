<?php if ( ! defined( 'WPINC' ) ) die;
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   FlowFlow
 * @author    Looks Awesome <email@looks-awesome.com>
 * @link      http://looks-awesome.com
 * @copyright 2014 Looks Awesome
 */
?>
<div id="fade-overlay" class="loading">
	<i class="flaticon-settings"></i>
</div>
<!-- @TODO: Provide markup for your options page here. -->
<form id="flow_flow_form" method="post" action="<?php echo $context['form-action']; ?>" enctype="multipart/form-data">
	<script>
		var _ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var la_plugin_slug_down = '<?php echo $context['slug_down']; ?>';
        var plugin_url = '<?php echo plugins_url() . '/' . $context['slug'] ; ?>';
		<?php if (isset($context['js-vars'])) echo $context['js-vars'];?>
	</script>
	<?php
		settings_fields('ff_opts');
		if (isset($context['hidden-inputs'])) echo $context['hidden-inputs'];
	?>
	<div class="wrapper">
		<?php
			if (FF_USE_WP) {
                echo '<h2>'. $context['admin_page_title'] . ' v. ' . $context['version'] . ' <a href="http://' . ( strpos($context['admin_page_title'], 'Stack') !== false ? 'stack' : 'flow' )  . '.looks-awesome.com/docs/Getting_Started" target="_blank">Documentation</a></h2>';
                echo '<div id="ff-cats">';
                wp_dropdown_categories(  );
                echo '</div>';
            }
		?>
		<ul class="section-tabs">
			<?php
				/** @var LATab $tab*/
				foreach ( $context['tabs'] as $tab ) {
					echo '<li id="'.$tab->id().'"><i class="'.$tab->flaticon().'"></i> <span>'.$tab->title().'</span></li>';
				}
				if (isset($context['buttons-after-tabs'])) echo $context['buttons-after-tabs'];
			?>
		</ul>
		<div class="section-contents">
			<?php
				/** @var LATab $tab*/
				foreach ( $context['tabs'] as $tab ) {
					$tab->includeOnce($context);
				}
			?>
		</div>
	</div>
</form>
<script>jQuery(document).trigger('html_ready')</script>
