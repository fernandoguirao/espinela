<?php
/**
 * Flow-Flow
 *
 * Plugin class. This class should ideally be used to work with the
 * public-facing side of the site.
 *
 * If you're interested in introducing administrative or dashboard
 * functionality, then refer to `FlowFlowAdmin.php`
 *
 * @package   FlowFlow
 * @author    Looks Awesome <email@looks-awesome.com>

 * @link      http://looks-awesome.com
 * @copyright 2015 Looks Awesome
 */
define('WPINC', true);
define('FF_USE_WP', false);

require_once( dirname($_SERVER["SCRIPT_FILENAME"]) . '/LAClassLoader.php' );
LAClassLoader::get(dirname($_SERVER["SCRIPT_FILENAME"]) . '/')->register(true);

/**
 * @param $object
 * @return string
 */
function var_dump2str($object){
	ob_start();
	var_dump($object);
	$output = ob_get_contents();
	ob_get_clean();
	return $output;
}

if (isset($_REQUEST['action'])){
	switch ($_REQUEST['action']) {
		case 'fetch_posts':
			$ff = FlowFlow::get_instance();
			$ff->processAjaxRequest();
			break;
		case 'load_cache':
			$ff = FlowFlow::get_instance();
			$ff->processAjaxRequestBackground();
			break;
		case 'refresh_cache':
			if (false !== ($time = FFDB::getOption('flow_flow_bg_task_time'))){
				if (time() > $time + 60){
					$ff = FlowFlow::get_instance();
					$ff->refreshCache();
					$time = time();
					FFDB::setOption('flow_flow_bg_task_time', $time);
					echo 'new cache time: ' . $time;
				}
			} else  FFDB::setOption('flow_flow_bg_task_time', time());
			break;
		case 'save_stream_settings':
			$admin = FlowFlowAdmin::get_instance();
			$admin->save_stream_settings();
			break;
		case 'get_stream_settings':
			$admin = FlowFlowAdmin::get_instance();
			$admin->get_stream_settings();
			break;
		case 'ff_save_settings':
			$admin = FlowFlowAdmin::get_instance();
			$admin->ff_save_settings_fn();
			break;
		case 'create_stream':
			$admin = FlowFlowAdmin::get_instance();
			$admin->create_stream();
			break;
		case 'clone_stream':
			$admin = FlowFlowAdmin::get_instance();
			$admin->clone_stream();
			break;
		case 'delete_stream':
			$admin = FlowFlowAdmin::get_instance();
			$admin->delete_stream();
			break;
		default:
			if (strpos($_REQUEST['action'], "backup") !== false) {
//				$snapshotManager = new FFSnapshotManager::get_instance();
//				$snapshotManager->processAjaxRequest();
			}
			break;
	}
}
die;