<?php
/*
  Plugin Name: DeMomentSomTres MailChimp Subscribe
  Plugin URI: http://demomentsomtres.com/english/wordpress-plugins/mailchimp-subscribe/
  Description:  MailChimp Subscription Management
  Version: 1.3.2
  Author: Marc Queralt
  Author URI: http://demomentsomtres.com/english/wordpress-plugins-by-demomentsomtres/
 */

define('DMST_MC_SUBSCRIBE_PLUGIN_URL', plugin_dir_url(__FILE__));
define('DMST_MC_SUBSCRIBE_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('DMST_MC_SUBSCRIBE_LANG_DIR', dirname(plugin_basename(__FILE__)) . '/languages');
define('DMST_MC_SUBSCRIBE_TEXT_DOMAIN', 'DeMomentSomTres-MailChimp-Subscribe');
define('DMST_MC_SUBSCRIBE_OPTIONS', 'dmst_mc_subscribe_options');

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
    exit;
}

require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
if (is_plugin_active('demomentsomtres-tools/demomentsomtres-tools.php')):
    require_once(ABSPATH . 'wp-content/plugins/demomentsomtres-tools/demomentsomtres-tools.php');
    require_once(ABSPATH . 'wp-content/plugins/demomentsomtres-tools/mailchimp/demomentsomtres-mailchimp.php');

    $demomentsomtres_mc_subscribe = new DeMomentSomTresMailchimpSubscribe;
endif;

class DeMomentSomTresMailchimpSubscribe {

    var $mcSession;
    var $loadJs = false;

    /**
     * @since 1.0
     */
    function DeMomentSomTresMailchimpSubscribe() {

        $this->mcSession = DeMomentSomTresTools::MailChimpSession(DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, 'API'));

        add_action('plugins_loaded', array(&$this, 'plugin_init'));
        add_action('admin_menu', array(&$this, 'admin_menu'));
        add_action('admin_init', array(&$this, 'admin_init'));
        add_action('widgets_init', array(&$this, 'register_widgets')) ;
        add_shortcode('demomentsomtres-mc-subscription', array(&$this, 'shortcode_general'));
        add_action('template_redirect', array(&$this, 'jsAdd'));
        add_action('wp_print_footer_scripts', array(&$this, 'jsLoad'));
        add_action('wp_ajax_nopriv_dms3mcsubscribe', array(&$this, 'ajax_subscribe'));
        add_action('wp_ajax_dms3mcsubscribe', array(&$this, 'ajax_subscribe'));
        add_action('wp_ajax_nopriv_dms3mcquery', array(&$this, 'ajax_query'));
        add_action('wp_ajax_dms3mcquery', array(&$this, 'ajax_query'));
        add_action('wp_ajax_nopriv_dms3mcsubscribebutton', array(&$this, 'ajax_subscribe_button'));
        add_action('wp_ajax_dms3mcsubscribebutton', array(&$this, 'ajax_subscribe_button'));
        add_action('wp_ajax_nopriv_dms3mcunsubscribebutton', array(&$this, 'ajax_unsubscribe_button'));
        add_action('wp_ajax_dms3mcunsubscribebutton', array(&$this, 'ajax_unsubscribe_button'));
    }

    /**
     * @since 1.0
     */
    function plugin_init() {
        load_plugin_textdomain(DMST_MC_SUBSCRIBE_TEXT_DOMAIN, false, DMST_MC_SUBSCRIBE_LANG_DIR);
    }

    /**
     * @since 1.0
     */
    function admin_menu() {
        add_options_page(__('Mailchimp Subscribe', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), __('Mailchimp Subscribe', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), 'manage_options', 'dmst_mc_subscribe', array(&$this, 'admin_page'));
    }

    /**
     * @since 1.0
     */
    function admin_page() {
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2><?php _e('DeMomentSomTres - MailChimp Subscribe', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?></h2>
            <form action="options.php" method="post">
                <?php settings_fields('dmst_mc_subscribe_options'); ?>
                <?php do_settings_sections('dmst_mc_subscribe'); ?>
                <br/>
                <input name="Submit" class="button button-primary" type="submit" value="<?php _e('Save Changes', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>"/>
            </form>
        </div>
        <div style="background-color:#eee;display:none;">
            <h2><?php __('Options', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?></h2>
            <pre style="font-size:0.8em;"><?php print_r(get_option(DMST_MC_SUBSCRIBE_OPTIONS)); ?></pre>
        </div>
        <?php
    }

    /**
     * @since 1.0
     */
    function admin_init() {
        register_setting('dmst_mc_subscribe_options', 'dmst_mc_subscribe_options', array(&$this, 'admin_validate_options'));

        add_settings_section('dmst_mc_subscribe_general', __('General Options', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_section_general'), 'dmst_mc_subscribe');
        add_settings_section('dmst_mc_subscribe_lists', __('Lists and groups', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_section_lists'), 'dmst_mc_subscribe');

        add_settings_field('dmst_mc_subscribe_apikey', __('Mailchimp API Key', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_field_apikey'), 'dmst_mc_subscribe', 'dmst_mc_subscribe_general');
        add_settings_field('dmst_mc_subscribe_doubleOptin', __('Subscription Double Optin', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_field_doubleOptin'), 'dmst_mc_subscribe', 'dmst_mc_subscribe_general');
        add_settings_field('dmst_mc_subscribe_welcomMessage', __('Subcription Welcome Message', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_field_welcomeMessage'), 'dmst_mc_subscribe', 'dmst_mc_subscribe_general');
        add_settings_field('dmst_mc_subscribe_showInitialList', __('Show Initial List', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_field_showInitialList'), 'dmst_mc_subscribe', 'dmst_mc_subscribe_general');
        add_settings_field('dmst_mc_subscribe_gaIntegration', __('Google Analytics Integration', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), array(&$this, 'admin_field_gaIntegration'), 'dmst_mc_subscribe', 'dmst_mc_subscribe_general');
    }

    /**
     * @since 1.0
     * @param array $input
     * @return array
     */
    function admin_validate_options($input = array()) {
        $newLists = array();
        foreach ($input['lists'] as $id => $list):
            if (isset($list['canSubscribe'])):
                $newLists[$id] = $list;
            endif;
        endforeach;
        $input['lists'] = $newLists;
        return DeMomentSomTresTools::adminHelper_esc_attr($input);
    }

    /**
     * @since 1.0
     */
    function admin_section_general() {
        
    }

    /**
     * @since 1.0
     */
    function admin_field_apikey() {
        $name = 'API';
        $value = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, $name);
        DeMomentSomTresTools::adminHelper_inputArray(DMST_MC_SUBSCRIBE_OPTIONS, $name, $value, array(
            'class' => 'regular-text'
        ));
    }

    /**
     * @since 1.0
     */
    function admin_field_doubleOptin() {
        $name = 'doubleOptin';
        $value = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, $name, FALSE);
        DeMomentSomTresTools::adminHelper_inputArray(DMST_MC_SUBSCRIBE_OPTIONS, $name, $value, array(
            'type' => 'checkbox',
        ));
        echo "<p style='font-size:0.8em;'>"
        . __('If checked double opt-in is used to subscribe users to Lists (not groups).', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        . "<br/><strong>"
        . __('WARNING: It is not recommended to use this feature as the user would not be able to subscribe to any group until the confirmation instructions are followed.', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        . "</strong>"
        . "</p>";
    }

    /**
     * @since 1.0
     */
    function admin_field_welcomeMessage() {
        $name = 'welcomeMessage';
        $value = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, $name, false);
        DeMomentSomTresTools::adminHelper_inputArray(DMST_MC_SUBSCRIBE_OPTIONS, $name, $value, array(
            'type' => 'checkbox',
        ));
        echo "<p style='font-size:0.8em;'>"
        . __('Determines if a welcome message is sent by MailChimp after a user is subscribed to a List.', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        . "<br/><strong>"
        . __('WARNING: It does not send a message if the user subscribes to a group that is not the first of the list.', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        . "</strong>"
        . "</p>";
    }

    /**
     * @since 1.1
     */
    function admin_field_showInitialList() {
        $name = 'shortcodeInitialList';
        $value = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, $name);
        DeMomentSomTresTools::adminHelper_inputArray(DMST_MC_SUBSCRIBE_OPTIONS, $name, $value, array(
            'type' => 'checkbox',
        ));
        echo "<p style='font-size:0.8em;'>" . __('Determines if an initial list is shown in the shortcode page before the user enters an email address', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</p>";
    }

    /**
     * @since 1.2
     */
    function admin_field_gaIntegration() {
        $name = 'gaIntegration';
        $value = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, $name);
        DeMomentSomTresTools::adminHelper_inputArray(DMST_MC_SUBSCRIBE_OPTIONS, $name, $value, array(
            'type' => 'checkbox',
        ));
        echo "<p style='font-size:0.8em;'>" . __('Each one of the buttons sends its own event to Google Analytics.', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        . "<br/><strong>"
        . __('WARNING: It only works with analytics.js. It does not work with ga.js.', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        . "</strong>"
        . "</p>";
    }

    /**
     * @since 1.0
     */
    function admin_section_lists() {
        echo '<p>' . __('Select which list allow user subscriptions and subscription parameters', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . '</p>';
        echo '<p><strong>' . __('It is not recommended to allow users to subscribe to lists and lists with groupings', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . '</strong></p>';
        $this->admin_fields_lists();
    }

    /**
     * @since 1.0
     */
    function admin_fields_lists() {
        $lists = DeMomentSomTresTools::MailChimpGetLists($this->mcSession, true);
        ?>
        <style>
            table.mc-subscribe td, table.mc-subscribe th {border-bottom:1px solid #333;}
        </style>
        <table class="mc-subscribe">
            <thead>
                <tr>
                    <th scope="column">
                        <?php _e('List', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                    </th>
                    <th scope="column">
                        <?php _e('Grouping', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                    </th>
                    <th scope="column">
                        <?php _e('Group', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                    </th>
                    <th scope="column">
                        <?php _e('Subscribers', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                    </th>
                    <th scope="column">
                        <?php _e('Can subscribe', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                    </th>
                    <th scope="column">
                        <?php _e('Name to show', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($lists as $list):
                    $listid = $list['id'];
                    $rowspan = count($list['groupings']) + 1;
                    if (isset($list['groupings'])):
                        foreach ($list['groupings'] as $grouping):
                            $rowspan2 = count($grouping['groups']);
                            $rowspan += $rowspan2;
                        endforeach;
                    endif;
                    ?>
                    <tr>
                        <td rowspan="<?php echo $rowspan; ?>">
                            <?php echo $list['name'] ?>
                        </td>
                        <td>
                            <?php _e('without group selection', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                        </td>
                        <td>
                            <?php _e('', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                        </td>
                        <td style="text-align:right;">
                            <?php echo $list['subscribers']; ?>
                        </td>
                        <td style="text-align:center;">
                            <?php
                            DeMomentSomTresTools::adminHelper_inputArray(
                                    DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-0-0]", 'canSubscribe', DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-0-0]", 'canSubscribe'), array('type' => 'checkbox')
                            );
                            ?>
                        </td>
                        <td>
                            <?php
                            DeMomentSomTresTools::adminHelper_inputArray(
                                    DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-0-0]", 'name', DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-0-0]", 'name', esc_attr($list['name']) . ' ' . __('without group selection', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)), array('class' => 'regular-text')
                            );
                            ?>
                        </td>
                    </tr>
                    <?php
                    if (isset($list['groupings'])):
                        foreach ($list['groupings'] as $grouping):
                            $rowspan2 = count($grouping['groups']) + 1;
                            $groupingid = $grouping['id'];
                            ?>
                            <tr>
                                <td rowspan="<?php echo $rowspan2; ?>">
                                    <?php echo $grouping['name']; ?>
                                </td>
                                <td style="display:none;">
                                    <?php _e('', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>
                                </td>
                                <td style="text-align:right;display:none;">
                                    &nbsp;
                                </td>
                                <td style="text-align:center;display:none;">
                                    &nbsp;
                                    <?php
//                                    DeMomentSomTresTools::adminHelper_inputArray(
//                                            DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-0]", 'canSubscribe', DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-0]", 'canSubscribe'), array('type' => 'checkbox')
//                                    );
                                    ?>
                                </td>
                                <td style="display:none;">
                                    &nbsp;
                                    <?php
//                                    DeMomentSomTresTools::adminHelper_inputArray(
//                                            DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-0]", 'name', DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-0]", 'name'), array('class' => 'regular-text')
//                                    );
                                    ?>
                                </td>
                            </tr>
                            <?php
                            foreach ($grouping['groups'] as $group):
                                $groupid = $group['id'];
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $group['name']; ?>
                                    </td>
                                    <td style="text-align:right;">
                                        <?php echo $group['subscribers']; ?>
                                    </td>

                                    <td style="text-align:center;">
                                        <?php
                                        DeMomentSomTresTools::adminHelper_inputArray(
                                                DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-$groupid]", 'canSubscribe', DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-$groupid]", 'canSubscribe'), array('type' => 'checkbox')
                                        );
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        DeMomentSomTresTools::adminHelper_inputArray(
                                                DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-$groupid]", 'name', DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS . "[lists][$listid-$groupingid-$groupid]", 'name', esc_attr($list['name']) . ': ' . esc_attr($group['name'])), array('class' => 'regular-text')
                                        );
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        endforeach;
                    endif;
                endforeach;
                ?>
            </tbody>
        </table>
        <?php
    }

    /**
     * @since 1.0
     * @return boolean
     */
    function register_widgets() {
        return register_widget("DeMomentSomTresMailchimpSubscribeWidget");
    }

    /**
     * @since 1.0
     * @return array the lists to which users can subscribe in format 'id' => 'name'
     */
    public static function validLists() {
        $lists = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, 'lists', array());
        $validLists = array();
        foreach ($lists as $id => $list):
            if ($list['canSubscribe']):
                $validLists[$id] = $list['name'];
            endif;
        endforeach;
        return $validLists;
    }

    /**
     * @since 1.0
     */
    public function jsRequired() {
        $this->loadJs = true;
    }

    /**
     * @since 1.0
     */
    public function jsAdd() {
        wp_enqueue_script('dms3mcsubscribe', plugin_dir_url(__FILE__) . 'js/dms3MCsubcribe.js', array('jquery'), '', true);
        $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
        $params = array(
            'ajaxurl' => admin_url('admin-ajax.php', $protocol),
            'ga' => ('on' == DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, 'gaIntegration')),
        );
        wp_localize_script('dms3mcsubscribe', 'dms3mcsubscribe', $params);
    }

    /**
     * @since 1.0
     */
    public function jsLoad() {
        if (!$this->loadJs):
            wp_deregister_script('dms3mcsubscribe');
        endif;
    }

    /**
     * @since 1.0
     */
    function ajax_subscribe() {
        $email = $_REQUEST['email'];
        $id = $_REQUEST['id'];
        $result = $this->subscribe($email, $id);
        echo $result['message'];
        die();
    }

    /**
     * @since 1.0
     */
    function ajax_query_tr_subscribed($id, $name, $message = '') {
        $result .= "<td class='name'>$name</td>";
        $result .= "<td>" . __('Subscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
        if ('' != $message):
            $result.="<span class='message'>$message.</span>";
        endif;
        $result.= "</td>";
        $result .= "<td>";
        $result .= "<i class='icon-refresh transition' style='display:none'></i>";
        $result .= "<input id='$id' class='btn btn-primary unsubscribe' type ='button' value='" . __('Unsubscribe', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "' />";
        $result .= "</td>";
        return $result;
    }

    /**
     * @since 1.0
     */
    function ajax_query_tr_notsubscribed($id, $name, $message = '') {
        $result .= "<td class='name'>$name</td>";
        $result .= "<td>" . __('Not subscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
        if ('' != $message):
            $result.="<span class='message'>$message.</span>";
        endif;
        $result .= "</td>";
        $result .= "<td>";
        $result .= "<i class='icon-refresh transition' style='display:none'></i>";
        $result .= "<input id='$id' class='btn btn-primary subscribe' type ='button' value='" . __('Subscribe', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "' />";
        $result .= "</td>";
        return $result;
    }

    /**
     * @since 1.0
     */
    function ajax_query() {
        $email = $_REQUEST['email'];
        $lists = $this->validLists();
        $result = '';
        if ('' == trim($email)):
            echo __('Email cannot be empty', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
            die();
        endif;
        $result .= "<h2>" . sprintf(__('Lists subscriptions for %s', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), $email) . "</h2>";
        $result .="<table class='table table-striped table-bordered table-hover'>";
        $result .= "<thead>";
        $result .= "<th>" . __('List', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</th>";
        $result .= "<th>" . __('Status', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</th>";
        $result .= "<th>" . __('Actions', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</th>";
        $result .= "</thead><tbody>";
        foreach ($lists as $id => $name):
            $result .= "<tr id='$id' key='$email'>";
            list($listid, $groupingid, $groupid) = explode("-", $id);
            if ($this->userSubscribed($email, $listid, $groupingid, $groupid)):
                $result.=$this->ajax_query_tr_subscribed($id, $name);
            else:
                $result.=$this->ajax_query_tr_notsubscribed($id, $name);
            endif;
            $result.="</tr>";
        endforeach;
        $result .= "</tbody></table>";
        echo $result;
        die();
    }

    /**
     * @since 1.0
     */
    function userSubscribed($email, $listid, $groupingid, $groupid) {
        $subscription = DeMomentSomTresTools::MailChimpGetEmailListSubscription($this->mcSession, $email, $listid, TRUE);
        if ($groupingid == 0):
            $result = ($subscription['subscribedToList'] == 1);
        else:
            if ($subscription['subscribedToList'] == FALSE):
                $result = FALSE;
            else:
                $result = FALSE;
                $groupName = DeMomentSomTresTools::MailChimpGetGroupName($this->mcSession, $listid, $groupingid, $groupid);
                foreach ($subscription['groupings'] as $grouping):
                    foreach ($grouping['groups'] as $group):
                        if ($group['name'] == $groupName):
                            $result = ($group['interested'] == 1);
                        endif;
                    endforeach;
                endforeach;
            endif;
        endif;
        return $result;
    }

    /**
     * @since 1.0
     */
    function ajax_subscribe_button() {
        $email = $_REQUEST['email'];
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $answer = $this->subscribe($email, $id);
        if ($answer['status'] == DeMomentSomTresTools::MAILCHIMP_SUCCESS):
            $result = $this->ajax_query_tr_subscribed($id, $name);
        else:
            $result = $this->ajax_query_tr_notsubscribed($id, $name, $answer['message']);
        endif;
        echo $result;
        die();
    }

    /**
     * @since 1.0
     */
    function ajax_unsubscribe_button() {
        $email = $_REQUEST['email'];
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $answer = $this->unsubscribe($email, $id);
        if ($answer['status'] == DeMomentSomTresTools::MAILCHIMP_SUCCESS):
            $result = $this->ajax_query_tr_notsubscribed($id, $name);
        else:
            $result = $this->ajax_query_tr_subscribed($id, $name, $answer['message']);
        endif;
        echo $result;
        die();
    }

    /**
     * @since 1.0
     */
    function subscribe($email, $id) {
        $status = DeMomentSomTresTools::MAILCHIMP_SUCCESS;
        $result = '';
        if ('' == trim($email)):
            $result.=__('Email cannot be empty', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
            $status == DeMomentSomTresTools::MAILCHIMP_ERROR;
        else:
            list($listid, $groupingid, $groupid) = explode("-", $id);
            $doubleOptin = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, 'doubleOptin', FALSE);
            $welcomeMessage = DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, 'welcomeMessage', FALSE);
            $groupings = array();
            if ($groupingid != 0):
                // A group is specified
                $groupName = DeMomentSomTresTools::MailChimpGetGroupName($this->mcSession, $listid, $groupingid, $groupid);
                $groupings[] = array(
                    'id' => $groupingid,
                    'groups' => array(
                        'name' => $groupName
                    ),
                );
            endif;
            $subscription = DeMomentSomTresTools::MailChimpGetEmailListSubscription($this->mcSession, $email, $listid, TRUE);
            if (!$subscription['subscribedToList']):
                $listSubscription = DeMomentSomTresTools::MailChimpSubscribeToList($this->mcSession, $listid, $email, $groupings, $doubleOptin, $welcomeMessage);
                if ($listSubscription['status'] == 'error'):
                    $result.=sprintf(__('Error while trying to subscribe %s,', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), $email);
                else:
                    $result .= sprintf(__('The email address %s has been subscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), $email);
                endif;
            else:
                $listGroupings = $subscription['groupings'];
                $newGroupings = array();
                foreach ($listGroupings as $ging):
                    $newGroups = array();
                    foreach ($ging['groups'] as $g):
                        if ($g['interested'] == 1):
                            $newGroups[] = $g['name'];
                            if (($ging['id'] == $groupingid) && ($g['name'] == $groupName)):
                                $alreadyExists = TRUE;
                            endif;
                        endif;
                    endforeach;
                    if (($groupingid == $ging['id']) && !isset($alreadyExists)):
                        $newGroups[] = $groupName;
                    endif;
                    $newGroupings[] = array(
                        'id' => $ging['id'],
                        'groups' => $newGroups,
                    );
                endforeach;
//                $result.='<pre>' . print_r($newGroupings, true) . '</pre>';
                if (!isset($alreadyExists)):
                    $updateResult = DeMomentSomTresTools::MailChimpUpdateMemberListGroups($this->mcSession, $listid, $email, $newGroupings);
                endif;
                $result .= sprintf(__('The email address %s has been subscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), $email);
            endif;
        endif;
        return array(
            'status' => $status,
            'message' => $result
        );
    }

    /**
     * @since 1.0
     */
    function unsubscribe($email, $id) {
        $status = DeMomentSomTresTools::MAILCHIMP_SUCCESS;
        $result = '';
        if ('' == trim($email)):
            $result.=__('Email cannot be empty', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
            $status = DeMomentSomTresTools::MAILCHIMP_ERROR;
        else:
            list($listid, $groupingid, $groupid) = explode("-", $id);
            $subscription = DeMomentSomTresTools::MailChimpGetEmailListSubscription($this->mcSession, $email, $listid, TRUE);
            if ($subscription['subscribedToList']):
                if ($groupingid == 0):
                    $unsubscribe = DeMomentSomTresTools::MailChimpUnsubscribeFromList($this->mcSession, $listid, $email);
                    if ($unsubscribe['status'] == DeMomentSomTresTools::MAILCHIMP_SUCCESS):
                        $status = DeMomentSomTresTools::MAILCHIMP_SUCCESS;
                        $result.=__('Unsubscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
                    else:
                        $status = DeMomentSomTresTools::MAILCHIMP_ERROR;
                        $result.=__('An error happened while unsubscribing', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
                    endif;
                else:
                    $groupName = DeMomentSomTresTools::MailChimpGetGroupName($this->mcSession, $listid, $groupingid, $groupid);
                    $numGroups = 0;
                    $newGroupings = array();
                    foreach ($subscription['groupings'] as $grouping):
                        $newGroups = array();
                        foreach ($grouping['groups'] as $group):
                            if ($group['interested'] == 1):
                                if ($group['name'] != $groupName):
                                    $numGroups++;
                                    $newGroups[] = $group['name'];
                                endif;
                            endif;
                        endforeach;
                        $newGroupings[] = array(
                            'id' => $grouping['id'],
                            'groups' => $newGroups
                        );
                    endforeach;
                    if ($numGroups == 0):
                        $unsubscribe = DeMomentSomTresTools::MailChimpUnsubscribeFromList($this->mcSession, $listid, $email);
                        if ($unsubscribe['status'] == DeMomentSomTresTools::MAILCHIMP_SUCCESS):
                            $status = DeMomentSomTresTools::MAILCHIMP_SUCCESS;
                            $result.=__('Unsubscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
                        else:
                            $status = DeMomentSomTresTools::MAILCHIMP_ERROR;
                            $result.=__('An error happened while unsubscribing', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
                        endif;
                    else:
                        $updateResult = DeMomentSomTresTools::MailChimpUpdateMemberListGroups($this->mcSession, $listid, $email, $newGroupings);
                    endif;
                endif;
            else:
                $result.=__('Trying to unsubscribe from a list you are not subscribed', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
                $status = DeMomentSomTresTools::MAILCHIMP_ERROR;
            endif;
        endif;
        return array(
            'status' => $status,
            'message' => $result
        );
    }

    /**
     * @since 1.0
     */
    function shortcode_general($atts) {
        $atts = shortcode_atts(array(), $atts);
        $this->jsRequired();
        $form = '';
        $form .= "<form action='#' method='post' id='dms3MCsubscribeGeneral' class='dms3MCsubscribeGeneral'>";
        $form .= "<div class='fase1'>";
        $form .= "    <label for='email'>" . __('Your email:', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</label>";
        $form .= "    <input class='widefat' id='email' name='email' type='text' value='' placeholder='" . __('Your email', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "'/>";
        $form .= "    <input class='btn btn-primary' type='submit' value='" . __('Verify subscriptions', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "' ></input>";
        $form .= "    <i class='icon-refresh transition' style='display:none'></i>";
        $form .= "</div>";
        $form .= "<p>" . __('If you put your email and click on &apos;Verify subscriptions&apos; you will see all your subscriptions and you could manage them.', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</p>";
        $form .= "<div class='fase2'>";
        if (DeMomentSomTresTools::get_option(DMST_MC_SUBSCRIBE_OPTIONS, 'shortcodeInitialList', TRUE)):
            $lists = $this->validLists();
            $form .= "<h2>" . __('Lists subscriptions', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</h2>";
            $form .="<table class='table table-striped table-bordered table-hover'>";
            $form .= "<thead>";
            $form .= "<th>" . __('List', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</th>";
            $form .= "<th>" . __('Status', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</th>";
            $form .= "<th>" . __('Actions', DMST_MC_SUBSCRIBE_TEXT_DOMAIN) . "</th>";
            $form .= "</thead><tbody>";
            foreach ($lists as $id => $name):
                $form .= "<tr id='$id''>";
                $form .= "<td>$name</td>";
                $form .= "<td>&nbsp;</td>";
                $form .= "<td>&nbsp;</td>";
                $form .="</tr>";
            endforeach;
            $form .= "</tbody></table>";
        endif;
        $form .= "</div>";
        $form .= "</form>";
        return $form;
    }

}

/**
 * @since 1.0
 */
class DeMomentSomTresMailchimpSubscribeWidget extends WP_Widget {

    /**
     * @since 1.0
     */
    function DeMomentSomTresMailchimpSubscribeWidget() {
        $widget_ops = array(
            'classname' => 'DMS3-mc-subscribe',
            'description' => __('Manages subscritions to lists and groups', DMST_MC_SUBSCRIBE_TEXT_DOMAIN)
        );
        $this->WP_Widget('DeMomentSomTresMCSubscribe', __('Subscribe and unsubscribe from a list', DMST_MC_SUBSCRIBE_TEXT_DOMAIN), $widget_ops);
    }

    /**
     * @since 1.0
     */
    function form($instance) {
        DeMomentSomTresMailchimpSubscribe::jsRequired();
        $title = esc_attr($instance['title']);
        $list = $instance['list'];
        $html = $instance['html'];
        $htmlbottom = $instance['htmlbottom'];
        $button = isset($instance['button']) ? $instance['button'] : __('Subscribe me', DMST_MC_SUBSCRIBE_TEXT_DOMAIN);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                <?php _e('Title:', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?> 
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('list'); ?>">
                <?php _e('Mailchimp List:', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?> 
            </label>
            <?php
            $selectoptions = DeMomentSomTresMailChimpSubscribe::validLists();
            ?>
            <select class="widefat" id="<?php echo $this->get_field_id('list'); ?>" name="<?php echo $this->get_field_name('list'); ?>">
                <option><?php _e('-- None --', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?></option>
                <?php
                foreach ($selectoptions as $id => $option):
                    ?>
                    <option value="<?php echo $id; ?>" <?php selected($id, $list) ?>><?php echo $option; ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('html'); ?>">
                <?php _e('Intro Text (can contain html tags):', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?> 
            </label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('html'); ?>" name="<?php echo $this->get_field_name('html'); ?>" cols="20" rows="16"><?php echo $html; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('htmlbottom'); ?>">
                <?php _e('Bottom Text (can contain html tags):', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?> 
            </label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('htmlbottom'); ?>" name="<?php echo $this->get_field_name('htmlbottom'); ?>" cols="20" rows="16"><?php echo $htmlbottom; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('button'); ?>">
                <?php _e('Button text:', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?> 
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('button'); ?>" name="<?php echo $this->get_field_name('button'); ?>" type="text" value="<?php echo $button; ?>" />
        </p>
        <?php
    }

    /**
     * @since 1.0
     */
    function update($new_instance, $old_instance) {
        $new_instance['title'] = strip_tags($new_instance['title']);
        return $new_instance;
    }

    /**
     * @since 1.0
     */
    function widget($args, $instance) {
        extract($args);
        extract($instance);
        echo $before_widget;
        $title = apply_filters('widget_title', $instance['title']);
        if ($title)
            echo $before_title . $title . $after_title;
        if ($html)
            echo $html;
        ?>
        <form action="#" method="post" id="form-<?php echo $this->id; ?>" class="dms3MCsubscribe">
            <input type="hidden" name="id" id="<?php echo $this->get_field_id('id'); ?>" value="<?php echo $list; ?>"/>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Your email:', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="email" type="text" value="" placeholder="<?php _e('Your email', DMST_MC_SUBSCRIBE_TEXT_DOMAIN); ?>" />
            <input class="btn btn-primary" type="submit" value="<?php echo $button; ?>" />
            <div class="messages"></div>
        </form>
        <?php
        if ($htmlbottom)
            echo $htmlbottom;
        echo $after_widget;
    }

}
?>